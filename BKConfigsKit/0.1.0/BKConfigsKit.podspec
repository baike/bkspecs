#
# Be sure to run `pod lib lint BKConfigsKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BKConfigsKit'
  s.version          = '0.1.0'
  s.summary          = 'iOS端-配置文件库'
  s.description      = '放置一些通用的配置属性类'

  s.homepage         = 'https://gitlab.com/baike/bkconfigskit.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Bai' => 'baike@persagy.com' }
  s.source           = { :git => 'https://gitlab.com/baike/bkconfigskit.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'BKConfigsKit/Classes/**/*'
  
  # s.resource_bundles = {
  #   'BKConfigsKit' => ['BKConfigsKit/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
